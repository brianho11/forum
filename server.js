var user_js = require('./user.js');
var post_js = require('./post.js');

var restify = require('restify');
var request = require('request');
var requestPromise = require('request-promise');

var localhost = "http://admin:admin@14.198.148.152:5984";
var server = restify.createServer();
server.use(restify.bodyParser());

server.use(restify.queryParser());
server.use(restify.authorizationParser());

server.listen(8080, function() {  
    console.log('Server start'); 
    // ------ User management -----------------------------------------------
    server.put(/^\/_users\/(\w+)$/, function(req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        console.log('PUT '+req.params[0]);
        var url = 'http://admin:admin@14.198.148.152:5984/_users/org.couchdb.user:'+req.params[0]

        request.get(url, function(err, response, body) {
            // Insert new user record
            if(response.statusCode == 404) {
                user_js.createUser(req, res, next);
            }
            //update exists user record
            if(response.statusCode == 200) {        
                user_js.updateUser(req, res, next,body);
            }  
        });

    });

    // handle request of delete user
    server.del(/^\/_users\/(\w+)$/, function(req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        user_js.deleteUser(req, res, next);
    });

    // handle request of get user
    server.get(/^\/_users\/(\w+)$/, function(req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        //http://admin:admin@14.198.148.152:5984/_users/org.couchdb.user:newuser
        user_js.showUser(req, res, next);       
    });
    // ------ End user management ---------------------------------------------


    // ------ Post management -------------------------------------------------

    server.put(/^\/(\w+)$/, function(req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        console.log("\n=== Post handler start ===");

        //console.log(req.authorization.scheme + " " + req.authorization.credentials);

        if(req.params.action == "newpost"){
            post_js.addPost(req, res, localhost);
        } else if (req.params.action == "updatepost") {
            post_js.updatePost(req, res, localhost);
        } else if (req.params.action == "newcomment") {
            post_js.addComment(req, res, localhost);
        } else {
            res.end();
        }
    });

    server.del(/^\/(\w+)$/, function(req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        post_js.deletePost(req, res, localhost);
    });

    server.get(/^\/(\w+)$/, function(req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', "*");
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type');
        res.setHeader('Access-Control-Allow-Credentials', true);
        post_js.getPost(req, res, localhost);
    });
    // ------ End post management ---------------------------------------------
});
