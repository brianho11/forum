exports.deleteUser = function(req, res, next) {
    deleteUser(req, res, next);
};

exports.showUser = function(req, res, next) {
    showUser(req, res, next);
};

exports.updateUser = function(req, res, next, body) {
    updateUser(req, res, next, body);
};

exports.createUser = function(req, res, next) {
    createUser(req, res, next);
};

var restify = require('restify');
var request = require('request');
var requestPromise = require('request-promise');


// method for create a new user
function createUser(req, res, next){
    var url = 'http://admin:admin@14.198.148.152:5984/_users/org.couchdb.user:'+req.params[0];
    var doc = {
        _id: "org.couchdb.user:"+req.params[0],
        name: req.params[0],
        password: req.params['password'],
        roles: [],
        dob:req.params['dob'],
        email:req.params['email'],
        gender:req.params['gender'],
        type: "user"
    }
                                                           
    var posturl = 'http://admin:admin@14.198.148.152:5984/'+req.params[0];
    var sort_doc = {
        "_id": "_design/sorting",
        "language": "javascript",
        "views": {
            "by_date": {
                "map": "function(doc) {\n  if(doc.createDate && doc.title){\n    emit(doc.createDate, {\"title\":doc.title, \"contents\":doc.contents});\n  }\n}"
            }
        }
    }
    
   
    console.log(doc);
   
    
    var pos_docStr = JSON.stringify(sort_doc);
    var pos_params = {
        uri:posturl + "/_design/sorting", 
        body : pos_docStr
    };


    var docStr = JSON.stringify(doc);



    var params = {uri: url, body: docStr};
     console.log(posturl);
   
    request.put(params, function(err, response, body) {
        if (err) {
            return next(new restify.InternalServerError('Cant create document'));
        }



        request.put(posturl, function(err, response, body) {
            request.put(pos_params, function(err, response, body) {
               
            });
        });

        res.write(body);
        res.end();
    });
    
    
    

}

// method for delete user 
function deleteUser(req, res, next){
    var url = 'http://admin:admin@14.198.148.152:5984/_users/org.couchdb.user:'+req.params[0];
    console.log("user to be delete: " + req.params[0]);

    requestPromise.get(url)
        .then(function(response) {
        response = JSON.parse(response);
        var url_w_rev = url + '?rev=' + response._rev;

        if(request.del(url_w_rev))
            res.write("Delete "+ req.params[0]+ " Success");
        else
            res.write("Fail");
        res.end();
    }).catch(function() {
        /*
             * If no record found
             */
        res.write("no record found");
        res.end();
    });
}


//method for update user  information
function updateUser(req, res, next, body){
    var url = 'http://admin:admin@14.198.148.152:5984/_users/org.couchdb.user:'+req.params[0]
    body = JSON.parse(body);
    var d = new Date();
    var date = d.toUTCString()
    body.dob = req.params['dob']
    body.email = req.params['email'];
    body.gender = req.params['gender'];
    body.login_pw = req.params['login_pw'];


    var params = {uri:url, body: JSON.stringify(body)};
    request.put(params, function(err, response, body) {
        console.log(body);
        if (err) {
            return next(new restify.InternalServerError('Cant create document'));
        }
        res.write(body);
        res.end();
        //res.send({items: req.params['items']});

    });
}

//method for show user recrod
function showUser(req, res, next){
    var url = 'http://admin:admin@14.198.148.152:5984/_users/org.couchdb.user:'+req.params[0];

    requestPromise.get(url)
        .then(function(response) {
        /*
        * If the record exists, return value
        */
        res.write(response);
        res.end();
    }).catch(function() {
        /*
        * If no record found
        */
        res.write("no record found");
        res.end();
    });
}