var request = require("request");
var restify = require('restify');

exports.getPost = function(req, res, localhost) {
    console.log("Executed: getPost");
    
    // Setup the response
    res.setHeader('Content-Type', 'application/json');
    
    if (!req.params.limit){
        // Create failure and send failure message.
        var resMsg = {
            'status': 'failure',
            'error': "Url Parameter 'limit' is required."
        };
        res.send(resMsg);
        return res.end();
    }
    
    var dbURL = localhost + "/" + req.params[0] + "/_design/sorting/_view/by_date?" +
        "descending=true&&" +
        "limit=" + req.params.limit;

    console.log(req.authorization.scheme + " " + req.authorization.credentials);
    
    var params = {
        uri: dbURL,
        headers: {
            'Authorization': req.authorization.scheme + " " + req.authorization.credentials
        }
    };
    
    request.get(params, function(err, response, body){
        if (err) {
            console.log(err);
            res.end();
            return next(new restify.InternalServerError('Cant create document'));
        }
        if (response.statusCode == 401) {
            res.send(JSON.parse(response.body));
                     return res.end();
        }
        //console.log(response);
        res.send(JSON.parse(body));
        res.end
    });
}

exports.addPost = function(req, res, localhost) {
    console.log("Executed: addPost");
    
    // Setup the response
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    
    if (!req.params["title"] || !req.params["contents"]){
        // Create failure and send failure message.
        var resMsg = {
            'status': 'failure',
            'error': 'JSON should have two attributes: title, contents'
        };
        res.send(resMsg);
        return res.end();
    }

    // Get username (database name)
    var username = req.params[0];
    
    // Blog database route
    var dbURL = localhost + "/" + username;
    console.log("dbURL: " + dbURL);
    
    // Get current date
    var date = new Date();
    var currentDate = date.getUTCFullYear() + "/" + 
        date.getUTCMonth() + "/" + 
        date.getUTCDate() + " " + 
        date.getUTCHours() + ":" + date.getUTCMinutes();
    
    // Prepare document for insert
    var doc = {
        title: req.params["title"],
        createDate: currentDate,
        lastModify: currentDate,
        contents: req.params["contents"],
        comment: new Array()
    };
    var docJSON = JSON.stringify(doc);
    
    // Create a parameter set for request
    var params = {
        uri: dbURL, 
        body: docJSON,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': req.authorization.scheme + " " + req.authorization.credentials
        }
    };

    // Insert record
    request.post(params, function(err, response, body) {
        if (err) {
            console.log(err);
            res.end();
            return next(new restify.InternalServerError('Cant create document'));
        }
        if (response.statusCode == 401) {
            res.send(JSON.parse(response.body));
            return res.end();
        }
        // Create success send success message
        var resMsg = {
            'status': 'success',
            'message': 'new post has been created',
            'data': doc
        };
        res.send(resMsg);
        return res.end();
    });
}

exports.updatePost = function(req, res, localhost) {
    console.log("Executed: updatePost");
    
    // Setup the response
    res.setHeader('Content-Type', 'application/json');
    
    if (!req.params['pid']){
        // Add failure and send failure message.
        var resMsg = {
            'status': 'failure',
            'error': 'JSON should have attribute pid'
        };
        res.send(resMsg);
        return res.end();
    }
    
    // Get username (database name) | post id (pid) 
    var username = req.params[0];
    var pid = req.params['pid'];
    
    // Blog database route
    var dbURL = localhost + "/" + username + "/" + pid;
    console.log("dbURL: " + dbURL);
    
    var params = {
        uri: dbURL,
        headers: {
            'Authorization': req.authorization.scheme + " " + req.authorization.credentials
        }
    };

    // Get current date
    var date = new Date();
    var currentDate = date.getUTCFullYear() + "/" + 
        date.getUTCMonth() + "/" + 
        date.getUTCDate() + " " + 
        date.getUTCHours() + ":" + date.getUTCMinutes();
    
    // Get doc by id
    request.get(params, function(err, response, body){
        if (err) {
            console.log(err);
            return next(new restify.InternalServerError('Cant get document'));
        }
        console.log(response.statusCode);
        if (response.statusCode == 404){
            res.send(JSON.parse(body));
            return res.end();
        }
        body = JSON.parse(body);
        body.lastModify = currentDate;
        // update element if requested
        if (req.params['title']){
            body.title = req.params['title'];
        }
        if (req.params['contents']){
            body.contents = req.params['contents'];
        }

        // Create a parameter set for request
        var params = {
            uri: dbURL, 
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': req.authorization.scheme + " " + req.authorization.credentials
            }
        };

        // Insert comment to the post document
        var bodyForMsg = body;
        delete bodyForMsg.comment;
        request.put(params, function(err, response, body){
            if (err) {
                console.log(err);
                res.end();
                return next(new restify.InternalServerError('Cant create document'));
            }
            if (response.statusCode == 401) {
                res.send(JSON.parse(response.body));
                         return res.end();
            }
            // Create success send success message
            var resMsg = {
                'status': 'success',
                'message': 'post ' + bodyForMsg.title + '(' + pid + ') has been update',
                'data': bodyForMsg
            };
            res.send(resMsg);
            res.end();
        });
    });
}

exports.deletePost = function(req, res, localhost) {
    console.log("Executed: deletePost");
    
    // Setup the response
    res.setHeader('Content-Type', 'application/json');
    
    if (!req.params['pid']){
        // Add failure and send failure message.
        var resMsg = {
            'status': 'failure',
            'error': 'JSON should have attribute pid'
        };
        res.send(resMsg);
        return res.end();
    }
    
    // Get username (database name) | post id (pid) 
    var username = req.params[0];
    var pid = req.params['pid'];
    
    // Blog database route
    var dbURL = localhost + "/" + username + "/" + pid;
    console.log("dbURL: " + dbURL);
    
    var params = {
        uri: dbURL,
        headers: {
            'Authorization': req.authorization.scheme + " " + req.authorization.credentials
        }
    };
    
    // Get doc by id
    request.get(params, function(err, response, body){
        if (err) {
            console.log(err);
            return next(new restify.InternalServerError('Cant get document'));
        }
        // if no post found, failure
        if (response.statusCode == 404) {
            var resMsg = {
                'status': 'failure',
                'error': 'no post has been found by id: ' + pid
            };
            res.send(resMsg);
            return res.end();
        } else if (response.statusCode == 401) {
            var resMsg = {
                'status': 'failure',
                'error': 'no post has been found by id: ' + pid
            };
            res.send(JSON.parse(body));
            return res.end();
        }
        
        // db url for delete with ver no.
        body = JSON.parse(body);
        var dbURL_w_ver = dbURL + '?rev=' + body._rev; 
        
        var params = {
            uri: dbURL_w_ver,
            headers: {
                'Authorization': req.authorization.scheme + " " + req.authorization.credentials
            }
        };
        
        // Delete the post
        var bodyForMsg = body;
        request.del(params, function(err, response, body){
            if (err) {
                console.log(err);
                res.end();
                return next(new restify.InternalServerError('Cant create document'));
            }
            if (response.statusCode == 401) {
                res.send(JSON.parse(response.body));
                         return res.end();
            }
            // Create success send success message
            var resMsg = {
                'status': 'success',
                'message': 'post ' + bodyForMsg.title + '(' + pid + ') has been deleted',
                'data': bodyForMsg
            };
            res.send(resMsg);
            res.end();
        });
    });
    
}

exports.addComment = function(req, res, localhost) {
    console.log("Executed: addComment");
    
    // Setup the response
    res.setHeader('Content-Type', 'application/json');
    
    if (!req.params['pid'] || !req.params['author'] || !req.params['comment']){
        // Add failure and send failure message.
        var resMsg = {
            'status': 'failure',
            'error': 'JSON should have three attributes: pid, author, comment'
        };
        res.send(resMsg);
        return res.end();
    }
    
    // Get blog name (database name) | post id (pid) | author | comment
    var blog = req.params[0];
    var author = req.params['author'];
    var pid = req.params['pid'];
    var comment = req.params['comment'];
    
    // Blog database route
    var dbURL = localhost + "/" + blog + "/" + pid;
    console.log("dbURL: " + dbURL);
    
    var params = {
        uri: dbURL,
        headers: {
            'Authorization': req.authorization.scheme + " " + req.authorization.credentials
        }
    };
    
    // Get current date
    var date = new Date();
    var currentDate = date.getUTCFullYear() + "/" + 
        date.getUTCMonth() + "/" + 
        date.getUTCDate() + " " + 
        date.getUTCHours() + ":" + date.getUTCMinutes();
    
    // Create comment obj
    var comment = {
        'author': author,
        'date': currentDate,
        'comment': comment
    };
    
    // Get doc by id
    request.get(params, function(err, response, body){
        if (err) {
            console.log(err);
            return next(new restify.InternalServerError('Cant get document'));
        }
        body = JSON.parse(body);
        body.comment.push(comment);
        
        // Create a parameter set for request
        var params = {
            uri: dbURL, 
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': req.authorization.scheme + " " + req.authorization.credentials
            }
        };
        
        // Insert comment to the post document
        request.put(params, function(err, response, body){
            if (err) {
                console.log(err);
                res.end();
                return next(new restify.InternalServerError('Cant create document'));
            }
            if (response.statusCode == 401) {
                res.send(JSON.parse(response.body));
                         return res.end();
            }
            // Create success send success message
            var resMsg = {
                'status': 'success',
                'message': 'new comment has been created',
                'data': comment
            };
            res.send(resMsg);
            res.end();
        });
    });
}